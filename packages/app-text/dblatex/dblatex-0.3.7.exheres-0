# Copyright (c) 2009 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge
require setup-py [ import=setuptools blacklist='3' multibuild=false ]

SUMMARY="Transform DocBook XML/SGML to LaTeX"
DESCRIPTION="
With dblatex you can:
 * transform a DocBook XML/SGML book or article to pure LaTeX,
 * compile the temporary LaTeX file with latex, pdflatex, or xelatex to produce DVI, PostScript and PDF files,
 * convert on the fly the figures included in the document,
 * have cross references with hot links,
 * olink to other documents built with dblatex,
 * write complex tables,
 * write several bibliographies,
 * reuse BibTeX bibliographies,
 * use callouts on program listings or on images,
 * create an index,
 * write mathematical equations in LaTeX,
 * write mathematical equations in MathML,
 * have revision bars,
 * customise the output rendering with an XSL configuration file,
 * use your own LaTeX style package.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        app-text/docbook-xml-dtd
        app-text/texlive-core [[ note = [ For makeindex ] ]]
        dev-libs/libxslt
        dev-libs/kpathsea [[ note = [ For kpsewhich ] ]]
        dev-texlive/texlive-basic
        dev-texlive/texlive-fontutils [[ note = [ For epstopdf ] ]]
        dev-texlive/texlive-fontsrecommended
        dev-texlive/texlive-latex [[ note = [ For latex and pdflatex ] ]]
        dev-texlive/texlive-latexextra
        dev-texlive/texlive-latexrecommended
        dev-texlive/texlive-mathextra
"

BUGS_TO="kimrhh@exherbo.org"

